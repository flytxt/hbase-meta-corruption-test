# README #

This is an integration test to prove Hbase issue https://issues.apache.org/jira/browse/HBASE-19331

### How to run this test ###

mvn clean verify -Dhbase.siteFiles.path=<path to your hbase and hadoop site files>

### What this test does ###

* Creates two tables SAMPLE_TBL_1 and SAMPLE_TBL_2
* Insert SAMPLE_TBL_1 one with long keys(8 bytes) having trailing byte = 0
* Split SAMPLE_TBL_1
* Insert SAMPLE_TBL_2 one with long keys(8 bytes) having trailing byte != 0
* Split  SAMPLE_TBL_2
* Verify start key and end key of regions from HRegionInfo if its byte array length is 8

### Results ###

* SAMPLE_TBL_1 fails the test
* SAMPLE_TBL_2 passes the test
