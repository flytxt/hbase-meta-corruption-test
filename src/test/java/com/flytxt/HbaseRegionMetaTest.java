package com.flytxt;

import static org.hamcrest.CoreMatchers.is;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HRegionInfo;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.MasterNotRunningException;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.ZooKeeperConnectionException;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(Parameterized.class)
public class HbaseRegionMetaTest {

    static {
        Logger root = LoggerFactory.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
        ((ch.qos.logback.classic.Logger) root).setLevel(Level.WARN);
        Logger flytxt = LoggerFactory.getLogger("com.flytxt");
        ((ch.qos.logback.classic.Logger) flytxt).setLevel(Level.INFO);
    }

    public static final int TTL = 60 * 60 * 24 * 10; // 10 days

    private static final long MAX_INSERT_COUNT = 10000;

    private static byte ZERO = 0;

    private static String TABLE_NAME_1 = "SAMPLE_TBL_1";

    private static String TABLE_NAME_2 = "SAMPLE_TBL_2";

    private static byte[] FAMILY = Bytes.toBytes("O");

    private static byte[] QUALIFIER = Bytes.toBytes("Q");

    private static Configuration configuration;

    private String tableName;

    @Rule
    public ErrorCollector collector = new ErrorCollector();

    @Parameterized.Parameters
    public static Collection<String[]> parameters() {
        return Arrays.asList(new String[][] { { TABLE_NAME_1 }, { TABLE_NAME_2 } });
    }

    public HbaseRegionMetaTest(String tableName) {
        this.tableName = tableName;
    }

    @BeforeClass
    public static void setup() throws MasterNotRunningException, ZooKeeperConnectionException, IOException, InterruptedException {
        configuration = HBaseConfiguration.create();
        try (HBaseAdmin admin = new HBaseAdmin(configuration);) {

            dropTable(admin, TABLE_NAME_1);
            dropTable(admin, TABLE_NAME_2);

            createTable(admin, TABLE_NAME_1);
            createTable(admin, TABLE_NAME_2);

            try (HConnection connection = HConnectionManager.createConnection(configuration);
                    HTableInterface table1 = connection.getTable(TableName.valueOf(TABLE_NAME_1));
                    HTableInterface table2 = connection.getTable(TableName.valueOf(TABLE_NAME_2))) {
                List<Put> puts = new ArrayList<Put>();
                log.info("Starting puts to table {}", TABLE_NAME_1);
                for (long i = 1, count = 0; count <= MAX_INSERT_COUNT; i++) {
                    byte[] key = Bytes.toBytes(i);
                    if (key[7] == ZERO) { // i am putting only numbers with trailing byte as 0 in TABLE_1
                        Put put = new Put(key);
                        put.add(FAMILY, QUALIFIER, key);
                        puts.add(put);
                        log.debug("Put to {}; Long value: {} ; Byte value : {}", TABLE_NAME_1, i, key);
                        count++;
                    }
                }
                table1.put(puts);
                table1.flushCommits();
                puts.clear();
                log.info("Puts complete .. lets split {}", TABLE_NAME_1);
                admin.split(TABLE_NAME_1);

                log.info("Starting puts to table {}", TABLE_NAME_2);
                for (long i = 1, count = 0; count <= MAX_INSERT_COUNT; i++) {
                    byte[] key = Bytes.toBytes(i);
                    if (key[7] != ZERO) { // i am putting only numbers without trailing byte as 0 in TABLE_1
                        Put put = new Put(key);
                        put.add(FAMILY, QUALIFIER, key);
                        puts.add(put);
                        log.debug("Put to {}; Long value: {} ; Byte value : {}", TABLE_NAME_2, i, key);
                        count++;
                    }
                }
                table2.put(puts);
                table2.flushCommits();
                log.info("Puts complete .. lets split {}", TABLE_NAME_2);
                admin.split(TABLE_NAME_2);
            }
        }
    }

    private static HColumnDescriptor createTable(HBaseAdmin admin, String tableName) throws IOException {
        HColumnDescriptor col = new HColumnDescriptor("O");
        col.setTimeToLive(TTL);
        log.info("Creating new table " + TABLE_NAME_1);
        HTableDescriptor tableDesc = new HTableDescriptor(TableName.valueOf(tableName));
        tableDesc.addFamily(col);
        admin.createTable(tableDesc);
        return col;
    }

    private static void dropTable(HBaseAdmin admin, String tableName) throws IOException {
        if (admin.tableExists(TableName.valueOf(tableName))) {
            log.info("Dropping table {}", tableName);
            admin.disableTable(tableName);
            admin.deleteTable(tableName);
        }
    }

    @Test
    public void testTableRegionStartAndEndKey() throws IOException, InterruptedException {

        try (HBaseAdmin admin = new HBaseAdmin(configuration)) {

            List<HRegionInfo> regionInfos = admin.getTableRegions(TableName.valueOf(tableName));
            while (regionInfos.size() < 2) {
                log.info("Region split for table {} yet to complete.. lets wait..", tableName);
                Thread.sleep(1000);
                regionInfos = admin.getTableRegions(TableName.valueOf(tableName));
            }
            log.info("region split complete .. Lets verify region infos for table {} ", tableName);
            for (HRegionInfo info : regionInfos) {
                log.info("===========================================================");
                log.info("Region Name : {}", info.getRegionNameAsString());
                log.info("Region Id :{}", info.getRegionId());
                log.info("Region start key :{}  , Key length :{}", info.getStartKey(), info.getStartKey().length);
                log.info("Region end key : {}  , Key length :{}", info.getEndKey(), info.getEndKey().length);
                log.info("-----------------------------------------------------------");
                if (info.getStartKey().length != 0) {
                    collector.checkThat("Region start key error for " + info.getRegionNameAsString(), info.getStartKey().length, is(8));
                }
                if (info.getEndKey().length != 0) {
                    collector.checkThat("Region end key error for " + info.getRegionNameAsString(), info.getEndKey().length, is(8));
                }
            }
        }

    }
}
